name := "GitHubUserContribution"

version := "0.1"

scalaVersion := "2.13.6"

val AkkaHttpVersion = "10.2.4"

libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.6.14"

libraryDependencies += "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion

libraryDependencies += "com.typesafe.akka" %% "akka-actor-typed" % "2.6.14"

libraryDependencies += "com.typesafe.play" %% "play" % "2.8.8"

libraryDependencies += "com.typesafe.play" %% "play-json" % "2.9.2"

libraryDependencies += "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion

libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.3"

libraryDependencies += "org.json4s" %% "json4s-native" % "4.0.0"

libraryDependencies += "com.github.sebruck" %% "akka-http-graphql" % "0.2.0"

libraryDependencies += "org.scalatestplus.play"   %% "scalatestplus-play" % "5.1.0" % Test