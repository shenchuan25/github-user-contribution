import org.scalatest.funspec.AsyncFunSpec
import play.api.libs.json.Json

class GitHubUserContributionSpec extends AsyncFunSpec  with TestHelper with IntegrationTestAssertions {
  describe(s"user contribution for org ministrycentered") {
    val offerJson = Json.parse(
      """
        |{
        |    "statusCode": "200 OK",
        |    "users": [
        |        "apstew",
        |        "bensie",
        |        "christianmccormick",
        |        "CleverClod",
        |        "cleverinventor",
        |        "cmoel",
        |        "danielma",
        |        "danielmurphy",
        |        "danott",
        |        "erikpedersen",
        |        "geolessel",
        |        "glosie",
        |        "jeremyricketts",
        |        "jessejanderson",
        |        "jremmen",
        |        "molawson",
        |        "mranallo",
        |        "seven1m",
        |        "sheck",
        |        "snappy316",
        |        "tage",
        |        "tannermares",
        |        "TheBerg",
        |        "WillDosSantos",
        |        "zhubert"
        |    ]
        |}
        |""".stripMargin
    )

    it("should successfully return 200 OK for /org/ministrycentered/contributors") {
      withClient { app => app.apiRequest(app.client, "/org/ministrycentered/contributors").get() }.map(response =>
        validateOKResponseWithContent(
          response,
          "/org/ministrycentered/contributors",
          offerJson,
          response.json
        )
      )
    }

    it("should successfully return 200 OK for /health") {
      withClient { app => app.apiRequest(app.client, "/health").get() }.map(response =>
        validateOKResponse(response, "/health")
      )
    }

    it("should successfully return 404 for wrong url like /badurl") {
      withClient { app => app.apiRequest(app.client, "/badurl").get() }.map(response =>
        validateNotFoundResponse(response)
      )
    }
  }
}
