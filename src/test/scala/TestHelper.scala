import akka.actor.ActorSystem
import akka.stream.Materializer
import org.scalatest.Assertion
import org.scalatest.matchers.should.Matchers
import play.api.http.ContentTypes.JSON
import play.api.http.HeaderNames.{AUTHORIZATION, CONTENT_TYPE}
import play.api.libs.ws.ahc.{AhcCurlRequestLogger, AhcWSClient}
import play.api.libs.ws.{WSClient, WSRequest, WSResponse}

import java.time.Clock
import java.util.concurrent.TimeUnit
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration

final case class ApplicationHook(client: WSClient, url: String) {

  def apiRequest(client: WSClient, path: String): WSRequest = {
    val headers = Seq(
      CONTENT_TYPE -> JSON,
      AUTHORIZATION -> "bearer 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"
    )
    client
      .url(url + path)
      .withRequestFilter(AhcCurlRequestLogger())
      .withHttpHeaders(headers: _*)
      .withRequestTimeout(FiniteDuration(10, TimeUnit.SECONDS))
  }
}

trait TestHelper {
  implicit val clock = Clock.systemUTC()
  implicit val system = ActorSystem("test")
  implicit val materializer = Materializer(system)

  def withClient[A](use: ApplicationHook => Future[A]): Future[A] = {
    val client = AhcWSClient()
    val applicationHook = ApplicationHook(client, "https://github-user-contribution.stage.com")

    use(applicationHook) andThen { _ => client.close() }
  }
}

trait IntegrationTestAssertions extends Matchers with TestHelper {
  def validateOKResponse(response: WSResponse, path: String): Assertion = {
    response.status should equal(200)
    response.uri.getPath should equal(path)
  }

  def validateOKResponseWithContent[T](
                                        response: WSResponse,
                                        path: String,
                                        responseExpected: T,
                                        responseContent: T
                                      ): Assertion = {
    response.status should equal(200)
    responseExpected should equal(responseContent)
    response.uri.getPath should equal(path)
  }

  def validateNotFoundResponse(response: WSResponse): Assertion = {
    response.status should equal(404)
  }

  def validateUserNotAuthorizedFoundResponse(response: WSResponse): Assertion = {
    response.status should equal(401)
  }

  def validateInternalServerErrorResponse(response: WSResponse): Assertion = {
    response.status should equal(500)
  }

}