package module

import com.typesafe.config.{Config, ConfigFactory}

class ReadConfig {
  val config:Config = ConfigFactory.load("application.conf")
}

object ReadConfig {
  val ghToken:Int = (new ReadConfig).config.getInt("app.gh-token")
}