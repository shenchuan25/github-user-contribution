package module

import java.util.concurrent.atomic.AtomicInteger
import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.server.{Directive0, Rejection}
import akka.http.scaladsl.server.Directives._

case class PathBusyRejection(path: Uri.Path, max: Int) extends Rejection

class RateLimiter(max: Int) {
    val concurrentRequests = new AtomicInteger(0)

    val limitConcurrentRequests: Directive0 =
      extractRequest.flatMap { request =>
        if (concurrentRequests.incrementAndGet() > max) {
          concurrentRequests.decrementAndGet()
          reject(PathBusyRejection(request.uri.path, max))
        } else {
          mapResponse { response =>
            concurrentRequests.decrementAndGet()
            response
          }
        }
      }
}
