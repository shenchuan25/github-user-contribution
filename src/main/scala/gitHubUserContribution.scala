import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.http.scaladsl.model.StatusCodes.{BadRequest, InternalServerError}
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.{Directives, ExceptionHandler, Route}
import spray.json.DefaultJsonProtocol
import play.api.libs.json._

import scala.io.StdIn
import scala.concurrent.Future
import scala.util.{Failure, Success}
import com.typesafe.scalalogging._
import module.{RateLimiter, ReadConfig}

object gitHubUserContribution {
  implicit val system = ActorSystem(Behaviors.empty, "gitHubUserContribution")
  implicit val executionContext = system.executionContext

  case class Users(statusCode:String, users:List[String])

  object PrettyJsonFormatSupport {
    import DefaultJsonProtocol._
    implicit val usersFormat = jsonFormat2(Users)
  }

  class JsonService extends Directives with LazyLogging {
    def getUsersContributions(statusCode: String, jsonStr: String): Users = {
      val json: JsValue = Json.parse(jsonStr)

      val users = json \\ "login"

      Users(statusCode, users.map(_.as[String]).toList)
    }

    val userContributionAPIExceptionHandler = ExceptionHandler {
      case ex: ArithmeticException =>
        extractUri { uri =>
          logger.error(s"Request to $uri could not be handled normally")
          complete(HttpResponse(BadRequest, entity = s"bad latitude and longitude number. Exception: ${ex.getMessage}"))
        }
      case ex: NullPointerException =>
        extractUri { uri =>
          logger.error(s"NullPointerException. Exception: ${ex.getMessage}")
          complete(HttpResponse(InternalServerError, entity = "Cannot read/write some of the field correctly"))
        }
      case ex: Exception =>
        extractUri { uri =>
          logger.error(s"Exception: ${ex.getMessage}")
          complete(HttpResponse(InternalServerError, entity = s"Exception: ${ex.getMessage}"))
        }
    }

    val limiter = new RateLimiter(max = ReadConfig.ghToken)

    val route: Route = handleExceptions(userContributionAPIExceptionHandler) {
      get {
        path("org" / Segment / "contributors") { case org_name =>
          limiter.limitConcurrentRequests {
            val responseMembers = getMembers(org_name)

            val responseContributions = getContributions("osantana")

            onSuccess(responseMembers) {
              case res: HttpResponse =>
                import PrettyJsonFormatSupport._
                import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
                complete(res.status, Unmarshal(res.entity).to[String].map(i =>
                  getUsersFromOrg(res.status.value, i)))
              case _ => complete("Failure")
            }
          }
        }
      }
    }
  }

  def main(args: Array[String]) = {
    val jsonService = new JsonService

    val bindingFuture = Http().newServerAt("localhost", 8080).bind(jsonService.route)
    StdIn.readLine()
    bindingFuture
      .flatMap(_.unbind())
      .onComplete(_ => system.terminate()) 
  }

  def getContributions(user:String): Future[HttpResponse] = {
    val authHeader   = RawHeader("Authorization", "bearer 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")
    val responseFuture: Future[HttpResponse] = Http().singleRequest(
      HttpRequest(
        method = HttpMethods.POST,
        uri = "https://api.github.com/graphql",
        entity = HttpEntity(ContentTypes.`text/plain(UTF-8)`,
          s"""
            | {"query": "{ viewer { login }}"}
         """.stripMargin)
      ).withHeaders(Seq(authHeader))
    )

    responseFuture
      .onComplete {
        case Success(res) =>
          val HttpResponse(statusCodes, headers, entity, _) = res
        case Failure(_)   =>
          sys.error("error")
      }
    responseFuture
  }

  def getMembers(org:String):Future[HttpResponse] = {
    val responseFuture: Future[HttpResponse] = Http().singleRequest(HttpRequest(uri = s"https://api.github.com/orgs/$org/members"))

    responseFuture
      .onComplete {
        case Success(res) =>
          val HttpResponse(statusCodes, headers, entity, _) = res
        case Failure(_)   => sys.error("error")

      }

    responseFuture
  }
}


